#!/usr/bin/env bash
# Fetch or update data, and build datasets the fastest we can.
# Demands a machine with lots of memory (3 to 4G) and a decent multi-threaded CPU (a i5 or equivalent, at least)

set -m  # Enable Job Control

source ~/configuration
source ~/python/bin/activate

LOGDIR=${net_lisias_retro_DATA}/file/search/cache

pkgs=${net_lisias_retro_file_ALL_ARCHIVES}
if [ $# -gt 0 ] ; then
	pkgs=$@
fi

java -cp net.lisias.retro.jar net.lisias.retro.java.util.python.Server&
javapid=$!

for f in $pkgs
do
		python3 -c " #${f}
import os
import net.lisias
net.lisias.DEBUG_MODE=False
import net.lisias.retro
net.lisias.retro.LOCAL_DATA_DIR=os.path.expanduser('${net_lisias_retro_DATA}')
import net.lisias.retro.file.miner
net.lisias.retro.file.miner.MINIMUM_COMPRESS_LEVEL=eval('${net_lisias_retro_file_miner_MININUM_COMPRESS_LEVEL}')
import net.lisias.retro.file.search
import net.lisias.retro.file.search.producer
net.lisias.retro.file.search.SERVED_ARCHIVES = eval(os.environ['net_lisias_retro_file_search_SERVED_ARCHIVES']) \
					if 'net_lisias_retro_file_search_SERVED_ARCHIVES' in os.environ else None
if net.lisias.retro.file.search.SERVED_ARCHIVES: net.lisias.retro.file.search.SERVED_ARCHIVES = set(net.lisias.retro.file.search.SERVED_ARCHIVES)
import net.lisias.retro.file.search.producer.${f} as m
net.lisias.Tasker.wait()
" 2> ${LOGDIR}/${f}.err 1> ${LOGDIR}/${f}.out &
done

# Wait for all parallel jobs to finish
#while [ 1 ]; do fg 2> /dev/null; [ $? == 1 ] && break; done
wait

kill -9 $javapid
