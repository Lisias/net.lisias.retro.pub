'''
Created on 19 de out de 2016

@author: lisias
'''

import os, distutils

if __name__ == '__main__':
	import net.lisias
	if 'net_lisias_task_LIMIT' in os.environ:
		net.lisias.Task.LIMIT=int(os.environ['net_lisias_task_LIMIT'])
	if 'net_lisias_DEBUG_MODE' in os.environ:
		net_lisias_DEBUG_MODE=distutils.util.strtobool(os.environ['net_lisias_DEBUG_MODE'])
	else:
		net_lisias_DEBUG_MODE=False

	import net.lisias.retro
	# Sets the pathname to where the cache will be placed. Must be readable and writable by the user running the daemon.
	if 'net_lisias_retro_DATA' in os.environ:
		net.lisias.retro.LOCAL_DATA_DIR=os.path.expanduser(os.environ['net_lisias_retro_DATA'])
	else:
		net.lisias.retro.LOCAL_DATA_DIR=os.path.expanduser('~/data')

	import net.lisias.retro.proxy
	net.lisias.retro.proxy.BIND_HOST=os.environ['BIND_HOST']
	net.lisias.retro.proxy.BIND_PORT=os.environ['net_lisias_retro_proxy_BIND_PORT']
	net.lisias.retro.proxy.HOST_NETLOC='%s:%s' % (os.environ['PUBLIC_ADDRESS'], net.lisias.retro.proxy.BIND_PORT)

	# A str to select the Routing Policy for local Providers:
	#	 INTERNET = Local Providers are proxied to clients. External Providers are redirected
	#				Use this to serve a pool of intranet Providers to the World without having to expose them.
	#				Usefull to serve a Farm of Providers
	#	 INTRANET = Local Providers are redirected to clients. External Providers are proxied.
	#				Use this to bridge foreign Providers to your Intranet.
	#	 None or anything else will disallow this Routing Policy.
	# net.lisias.retro.proxy.LOCAL_ROUTING_POLICY='INTERNET'
	net.lisias.retro.proxy.LOCAL_ROUTING_POLICY='none'

	import net.lisias.retro.proxy.WS
	net.lisias.retro.proxy.WS.run()
