'''
Created on 15 de nov de 2016

@author: lisias
'''

import os, urllib.request, shutil

LOCAL_DATA_DIR=os.path.expanduser('~/data')

def fetch_file() -> None:
	local_filename = os.path.join(LOCAL_DATA_DIR, 'file/search/cache/wos')
	os.makedirs(local_filename, exist_ok=True)
	local_filename = os.path.join(local_filename, 'ls-lR')

	url = "%s://%s/%s/%s" % ('https', 'wos.meulie.net', '/pub/sinclair', 'ls-lR')

	request = urllib.request.Request(url)
	request.add_header('Host', 'wos.meulie.net')
	request.add_header('User-Agent', 'Mozilla/5.0 (Android 5.1.1; Mobile; rv:49.0) Gecko/49.0 Firefox/49.0')
	request.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
	request.add_header('Accept-Language', 'en-US,en;q=0.5')
	request.add_header('Accept-Encoding', 'gzip, deflate')
	request.add_header('Connection', 'keep-alive')
	request.add_header('Upgrade-Insecure-Requests', '1')
	with urllib.request.urlopen(request) as response:
		with open(local_filename, "wb") as file:
			shutil.copyfileobj(response, file)

if __name__ == '__main__':
	print ("Fetching ls-LR from WoS mirror...")
	fetch_file()
	print ("Done!")
