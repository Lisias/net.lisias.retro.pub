# Updating and Upgrading your box

The most recent release of the Confederation is always tested against the most recent versions of the supported Distros.

I do not (re)test it against updated distros, neither vice-versa.

## Base System

### Raspian

	sudo apt-get update -y
		(check if everything works fine)
	sudo apt-get upgrade -y       *****
		(check again)
	sudo apt-get dist-upgrade -y
		(and one time more)
	sudo rpi-update

These triple check is desirable, as if anything goes wrong, the search scope is nailed down to fewer packages. Most of the time, however, everything will just works.

(but don't hurt to DD the SD image to your computer before updating it! Just in case...)

### Others

TODO

## Confederation

	sudo -Hiu srv
	cd net.lisias.retro/pub/
	git fetch ; git reset --hard origin/master
	source ~/python/bin/activate
	easy_install net.lisias.retro.WS-1.1.0-py3.4.egg
		(or the the one you find there!)

You *must* manually review your `~/configuration` file against the `net.lisias.retro/pub/configuration` one. A automated merge tool is planned, but not available in the short term.
