# Installing net.lisias.retro.WS

The easyest way to fire up a node is using a Raspberry Pi: download the SD Image available [here](http://service.retro.lisias.net), unzip it and copy the image into a 2Gb (or bigger) SD Card, as instructed [here](https://www.raspberrypi.org/documentation/installation/installing-images/README.md). My image is based on the Raspbian Jessie Lite.

That made, go to the [CONFIGURE.md](./CONFIGURE.md) document to configure the desired services and fire up the daemons.

If you choose to maintain your own box (what is not a bad idea), this document will guide you into the process.

## Requirements

* A UNIX-like box.
	- MacOS
	- BSD
	- Linux
	- Windows is not supported, but I don't see a reason it would not work on it if all the requirements below are met.
* Memory
	- 256MbB is the bare minimum. Less than that and you will end up serving half a service
	- 512MB is recommended, however. But some bigger Search Engines will not cope with this.
	- 3072MB is more than enough for everything at the moment
	- If using a Raspberry Pi, make sure to setup the GPU memory to the minimum (16Mb on RPi Model 1).
		- http://raspberrypi.stackexchange.com/questions/673/what-is-the-optimum-split-of-main-versus-gpu-memory
* About 1GB of dedicated storage
	- Less if you don't plan to serve WebRadio or Databases
* CPython 3.4
	- And must be CPython 3.4 !
		- It's the less old =P version for Raspian Jessie at the moment.
		- PyPy support is Work in Progress
	- PIP
	- VIRTUALENV
* GIT
	- So you can checkout this repo! :-)
* Screen
	- I didn't daemonized the services. And probably never will.
	- This will allow you to connect to a Screen and monitor the daemons by hand
* Mini UPNP Client
	- [http://miniupnp.free.fr/files/](http://miniupnp.free.fr/files/)
	- Needed if your appliance(s) is(are) under NAT

## Step by step

All of this is pretty vanilla, but since each Linux Distribution have its own idiosyncrasies, I can't guide you for every one of them. Once your box is configured, go to [CONFIGURE.md](./CONFIGURE.md) to set up the Services.

Follow the instructions for the distros I have direct contact nowadays.

### Premade Images

You can find premade SD images [here](http://service.retro.lisias.net/#pre-made-working-environment). This would save you from the most part of the weight-lifting.

Use the very same procedures you would use for flashing a SD for Raspian (explained [here](https://www.raspberrypi.org/documentation/installation/installing-images/README.md). Don't forget to [expand the filesystem](https://www.raspberrypi.org/documentation/configuration/raspi-config.md) if your SD image is larger than 2GB.

### Gentoo

Yes, I use Gentoo. For a systemD-less life. :-)

Create a user **with home** but without login privileges. I will assume the username **srv**, group **srv** for the rest of this document.

Login into your box using a SUDO capable account and:

	sudo useradd -m -U -r srv
	sudo emerge =dev-lang/python-3.4.3-r1
	sudo emerge dev-python/virtualenv
	sudo emerge git
	sudo emerge screen

There's no portage for the MiniUPNP, so we need to compile it ourselves.

	TODO

Now, we will sudo to the **srv** user to install the Services.

	sudo -u srv bash
	cd ~

Make sure that **srv** can not sudo as root:

	sudo -i

And use the root password. _***It must be refused***_.

See the [COMMON](#common) section to finish the job.


### Raspian (and Debian) from scratch

If you already have your Raspian installed and configured on you Raspberry PI, or will run the Services on a Debian box, do this:

	sudo apt-get update -y
	sudo apt-get upgrade -y
	sudo apt-get dist-upgrade -y

For Raspian only:

	sudo apt-get install rpi-update
	sudo rpi-update

This will update and upgrade your system. Now you need to install Python and some more support libraries:

	sudo apt-get install python3.4 virtualenv python3.4-dev
	sudo apt-get install git screen miniupnpc realpath
	sudo apt-get install libarchive13 libarchive-dev

Now, create a user **with home** but **without login** privileges. I will assume the username **srv**, group **srv** for the rest of this document. To do so, login into your box using a SUDO capable account and:

	sudo useradd -m -U -r srv

Now, we will sudo to the **srv** user to install the Services.

	sudo -Hiu srv

Make sure that **srv** can not sudo as root:

	sudo -i

And use the root password. _***It must be refused***_. If not, ask for help.

See the [COMMON](#common) section to finish the job.


### COMMON (all of them)

Things are easier now. Using your dedicated service user (`sudo -Hiu srv`), do:

	virtualenv --python=/usr/bin/python3.4 python
	source ./python/bin/activate
	pip install libarchive-c
	git clone --depth 1 https://bitbucket.org/Lisias/net.lisias.whistleblower.git
	mkdir net.lisias.retro
	cd net.lisias.retro
	git clone --depth 1 https://bitbucket.org/Lisias/net.lisias.retro.pub.git pub
	cd pub
	easy_install net.lisias.retro.WS-1.1.0-py3.4.egg

And that's it. The egg file will install all the other dependencies on the virtual environment, as well the Services (it's a looong process, be patient). Please note that the egg filename will change overtime, as new releases are made.

## Configuring the Auxiliary Services

### The Whistle Blower

One nasty problem to be dealt when you publish services on the Wild, Wide, Web are the phishers. There's a lot, and I really mean a lot, of servers those only function in life is to scan every other server they can find in search of some vulnerability or edge to exploit. They succeed now and then, but most of the time they just waste your bandwidth, your processing power and your time.

In order to cope with them, I maintain a black list on [report.lisias.net](http://report.lisias.net]) that can be used to feed a firewall with such bastards. The Whistle Blower is a tool to do that.

It's not a protection, it's just a measure to reduce the load from your server(s) by telling your server who hit mine. By no means blindly thrust this tool, sometimes it's just a Web Crawler being just a bit too much aggressive.

That said, this tool downloads from my server a list of potential bad guys that hit my servers more that 100 times, or in the last week. This is enough to get rid of the more aggressive scanner without being too harsh with the occasional offender - and since IPs change owners regularly, it's not advisable to be too restrictive. There's a WHITE-LIST file where you can list some IPs that you want to be free of harassment by the tool - Google's crawlers can be, now an then, a bit intense on their scanning for updates and sometimes they trigger my Whistle Blower - you may want to white list them.

And, most important of all, you **MUST** put your IPs on the WHITE LIST to be sure that you will not be locked out of your servers if by bad luck you get an IP that hit me in the past.

So, first things first. If you decide to use this tool (you can live without it! Your servers will suffer, but they will survive...) log in in your server using a SUDO capable account and:

	sudo iptables -L

This will list the current IPTABLES rules. What we will do now is to create the CHAIN the Whistle Blower uses, add it to the INPUT chain, and then we will create a overrule that will guarantee that your IP will be always allowed, no matter what the other chains tell.

	sudo iptables -N INPUT.block.temp
	sudo iptables -t filter -A INPUT -j INPUT.block.temp

The new chain is empty, so it's harmless.

Now, using a browser on your home computer, check the IP you are using at the moment by visiting [this site](https://www.iplocation.net/find-ip-address). Please note the "Your IP Address is" info, and keep the browser opened - you will need it later.

Back to the IPTABLEs, type now:

	sudo iptables -I INPUT 1 -s 111.222.333.444  -j ACCEPT
	sudo iptables -L
	pi@rpi:~ $ sudo iptables -L
		Chain INPUT (policy ACCEPT)
		target     prot opt source               destination
		ACCEPT     all  --  blablablabal.virtua.com.br  anywhere
		INPUT.block.temp  all  --  anywhere             anywhere

Where 111.222.333.444 is the IP you got from the iplocation.net , and blabalbalabl.virtua.com.br will be the reverse DNS from your Internet Provider to your IP (it's a "name" they give to your IP). Be sure that the rule that handles your IP is listed **before** the INPUT.block.temp one. If by any reason you don't get this, delete the rule and ask for help as folows:

	sudo iptables -D INPUT -j INPUT.block.temp
	sudo iptables --flush INPUT.block.temp
	sudo iptables --delete-chain INPUT.block.temp

From now on, even if your IP get tracked on my servers, your IPTABLEs will guarantee that you will be accepted in your own servers. :-)

Now, if you decided to keep the tool, we will create a rule on the Daily CRONTAB to feed the temporary blacklist every day:

	sudo ln -s /home/srv/net.lisias.whistleblower/clients/iptables/ipban.cronjob.sh /etc/cron.daily/ipban.cronjob

And that's it.

You can keep an eye on who is being blocked at the moment by peeking the tool's log as follows:

	sudo less /var/log/ipban.log


## The net.lisias.retro Services

The initial configure for the Services is a lot easier, but you will need a text editor. Some people use VI to do that, but there's a more user friendly tool called MCEDIT, embedded with the Midnight Commander package. If you remember WordStar =P you will be comfortable with this tool.

Anyway, I will use VI as every distro has it by default.


### COMMON

Most of the configuration options are common for all the services. To simplify things later, I made it a bit more complicated at first.

You need to edit the `.profile` file on the `srv` user home directory to include these common configurations on the environment every time this user is logged.

So:

	sudo -Hiu srv
	cp ~/net.lisias.retro/pub/configuration ~/
	vi ~/.profile

and type, at the end of the file, `source ~/configuration` as follows. Use the navigation keys to go to the end of the file, press 'i' to enter in insert mode, type the text, hit ESC, and then `:wq` to save and exit.

```bash
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		. "$HOME/.bashrc"
	fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

source ~/configuration
~
~
~
~
~
"~/.profile" 22 lines, 675 characters
```


### The WebRadio

Build a Web Radio is easy. Install [Music Player Daemon](https://www.musicpd.org/) and [IceCast2](http://icecast.org/), edit the MPD conf file and that's it. There're a lot of tutorials out there, so I will not repeat that here.

Once you got MPD running, you will need to link the Service's data cache into the MPD's music repository. Using a SUDO capable account:

	mpc status
			(to confirm the tool is available and the daemon running)
	sudo -Hiu srv
	cd ~
			(tilde)
	mkdir -p data/file/mpd/cache/AmigaMod
	exit
	sudo -i
	cd /var/lib/mpd/music
	chgrp audio home/srv/data/file/mpd/cache/
	ln -s /home/srv/data/file/mpd/cache/webradio ./AmigaMod
	mpc update

This command sequence will create a symbolic link on the MPD music repository to the download cache of the WebRadio, virtually extending the MPD directory into it. These files must be readable by the MPD, so I changed the directory group to MPD's one, `audio`. The `mpc update` tells MPD to rescan its music repository.

Copy some music (MP3, MPD, OGG, anything) into `/var/lib/mpd/music` and

	mpc update
	mpc ls | mpd add
	mpc play

And your webradio is in the air!

You can use `mpc next` and `mpc prev` to switch musics, and `mpc stop` to shut the radio down.

The present Service only serves Amiga Module files, and you will need to copy the HTML, JS and CSS files from http://service.retro.lisias.net/search/ and copy them to your own HTTP server (how about using [http2.bash](https://bitbucket.org/Lisias/httpd2.bash)? It's ugly, it's slow, but it's probably one of the lightest HTTP servers available out there). Edit the `ws.js` to pinpoint your own servers and you will have a HTML interface to program your WebRadio's playlist.

The `CONFEDERATION_PROXIES` option (in the runner itself) should be commented out for this service (using # on the beginning of the line). Web Radios are **singleton services**, i.e, there can be only one of each in the whole Confederation. But nothing prevents you to use a private one, right? You just can't register it on the Confederation, as I already have this service online myself and yours will be refused.

New WebRadios (using different archives/entities naming) can be confederated however - so it's possible to have different "channels" available, or whole new Radios. More is to come in the near future.

We finished configuring this service. type `:wq` on the VI terminal.

Be aware, however: serving a audio stream consumes approximately half of the computing power of a Raspberry PI 1 Model B.

It's usually not a big deal, but if you plan to use your RPi for something else, things can get slow sometimes. I use mine to serve a WebRadio *and* some Search Engine archives, and I have no problems but the loading times.

Multicore appliances should handle this better, however.


## NAT

You can read more about NAT [here](https://pt.wikipedia.org/wiki/Network_address_translation), but in a nutshell: since IPv4 are scarce nowadays, a way to save IPs is to use a Public IP on the network's gateway to the Internet, and everybody else in the intranet uses a local IP (something in the 192.168.xxx.xxx range). Such IPs are not routable, i.e., machines with such IPs cannot talk to the Internet. Only the NAT server can. Since every intranet in the world can reuse the same local IPs, we end up using a far less public IPs.

So, when someone on the intranet needs to talk to someone on the Internet, it asks NAT to do so in his behalf. NAT does the request, and then echoes back it to you. It forges the IP's address fields to make you feel as you are talking directly to the Internet, but in reality, you don't. It's messy, but it works.

Problem is that... In the same way your intranet machines can't talk to the outer world, machines out there can't talk to the intranet neither. They can talk only to NAT.

So, in order to serve requests to the World, you must tell NAT to redirect requests from out there to a specific machine (or some machines) in your intranet. One by one basis.

So, and by example, to allow my RPI to serve HTTP2.BASH, I had to tell my router (that is also the NAT): "hey, everybody someone out there hits your port 80, forward the request to the RPI machine at 192.168.100.150 in his port 8080". The guys out there will be talking to my router at port 80, but internally, the requests are served by the RPI at port 8080. Please note that the whole intranet must share the NAT ports (as it's the only machine exposed to the Internet), so I can only have *ONE* machine in my intranet serving HTTP using the NAT's port 80. I can have as many machines serving HTTP/80 in my intranet as I want, but only one of them can receive forwards from the 80 port in the NAT at the time.

This can be somewhat confusing, but it's simple when you get used to it. Think on your hotel's room when you travel: nothing prevents your room to have the same number as the Hotel's street address, but it can happens only once (there can't be two rooms with the same number), and this should not prevent your room from being serviced.


### UPNP

So, in order to serve to the Internet, I need to talk to that NAT thing. Ok. But what language it talks?

There's a bunch. *Zeroconf*, *Bonjoir* and *Universal Plug and Play* are the most used of them. Good Wifi Routers talks more than one, by the way.

UPNP is the one that most open source routers supports, and most commercial ones too - so it's the one I adopt in this document. The MiniUPNPC program is the command line utility that we will use on this project.

If you stick with the default ports, you don't have do do anything - the configuration file above it's all you need to change. But if you decide to change (or add) things, this is what you need to know:

	upnpc -a <LOCAL_IP> <LOCAL_PORT> <EXTERNAL_PORT> <protocol>

This command will tell NAT that every time it receives a request that hits EXTERNAL\_PORT using the PROTOCOL (tcp, udp, icmp, etc), he will forward the request to LOCAL\_IP:LOCAL\_PORT intranet address. As a concrete example:

	upnpc -a RPI 8080 80 tcp
	upnpc -a RPI 8000 8000 tcp

This is telling NAT to forward all HTTP requests (port 80 tcp) to the RPI's 8080, and all the 8000 requests (WebRadio) to the RPI's 8000 (where RPI is the name of a host).  Since usually there's only *one* NAT server on the intranet, there's no need to specify him on the command line - upnpc will find it by itself.

There's a handy utility to easily configure UPnP router. See below.

## Utilities

Some extra tools are provided to fill a gap or two.

### run_config_nat.sh

This little hack automates the UPnP setup in your intranet. It's executed at system's startup - you will need to run it again, manually, if you reboot your router!

See the `~/configuration` file on `srv`'s home directory for details.

### run_wos_init.py

World Of Spectrum is a troubled site. For years, it was ***THE*** reference for the Sinclair computing, but nowadays its future must be considered uncertain.

The FTP service was shutdown for good, but a HTTP mirror was created. However, the mirror maintainer choose to impose some restrictions in order to save his server's resources. One of these restrictions is to ban non browsers access to the repository.

Such measure is easy to countermeasure, but I choose not to implement such countermeasures on the Services itself. The guy have his reasons.

Since the WoS repository is freezed anyway, there's no need to keep updating the data (I flagged it as "dead" on the code - a repo that is not updated anymore).

You can perfectly download the FTP list file using your browser, and then copy it to the right place using SCP. But I choose to save you the trouble, implementing this one shot utility to do it for you. It will download the file pretending it is a Firefox Browser running on Android, what will tricks the server into serving it.

Please don't abuse the guy's server. Don't use this tool to mass download the WoS files. The guy will just shut you down, or worse, shut the server down.

To initialize the WoS repository, using a SUDO capable account in your appliance:

	sudo -Hiu srv
	cd net.lisias.retro/pub
	python3 ./run_wos_init.py

And that it. After this tool is run, you can fire up the Search Engine for WoS.

NOTE: Currently, the last known official mirror of WoS is down. This utility will not work properly until a new one is found.
