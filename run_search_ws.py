'''
Created on 19 de out de 2016

@author: lisias
'''

import os, distutils

if __name__ == '__main__':
	import net.lisias
	if 'net_lisias_task_LIMIT' in os.environ:
		net.lisias.Task.LIMIT=int(os.environ['net_lisias_task_LIMIT'])
	if 'net_lisias_DEBUG_MODE' in os.environ:
		net_lisias_DEBUG_MODE=distutils.util.strtobool(os.environ['net_lisias_DEBUG_MODE'])
	else:
		net_lisias_DEBUG_MODE=False

	import net.lisias.retro
	net.lisias.DEBUG_MODE=False
	net.lisias.retro.CANNONICAL_DATASET_REPOSITORY_ACTIVE=distutils.util.strtobool(os.environ['net_lisias_retro_CANNONICAL_DATASET_REPOSITORY_ACTIVE'])

	# Sets the pathname to where the cache will be placed. Must be readable and writable by the user running the daemon.
	if 'net_lisias_retro_DATA' in os.environ:
		net.lisias.retro.LOCAL_DATA_DIR=os.path.expanduser(os.environ['net_lisias_retro_DATA'])
	else:
		net.lisias.retro.LOCAL_DATA_DIR=os.path.expanduser('~/data')


	import net.lisias.retro.file.search
	net.lisias.retro.file.search.BIND_HOST=os.environ['BIND_HOST']
	net.lisias.retro.file.search.BIND_PORT=os.environ['net_lisias_retro_file_search_BIND_PORT']
	net.lisias.retro.file.search.HOST_NETLOC='%s:%s' % (os.environ['PUBLIC_ADDRESS'], net.lisias.retro.file.search.BIND_PORT)

	from net.lisias.retro.file.search import Annunciator
	# Add well known Proxy URLs to the Confederation set.
	# The WS will try to register itself in each Proxy.
	p=eval(os.environ['net_lisias_retro_CONFEDERATION_PROXIES'])
	if p:
		if not isinstance(p, list):
			raise RuntimeError("net_lisias_retro_CONFEDERATION_PROXIES must be a list")
		Annunciator.CONFEDERATION_PROXIES |= set(p)
	del p

	d = eval(os.environ['net_lisias_retro_file_search_SERVED_ARCHIVES'])
	if d:
		if not isinstance(d, list):
			raise RuntimeError("net_lisias_retro_file_search_SERVED_ARCHIVES must be a list")
		net.lisias.retro.file.search.SERVED_ARCHIVES=set(d)
	del d

	import net.lisias.retro.file.search.WS
	net.lisias.retro.file.search.WS.run()
