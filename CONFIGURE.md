# Configuring the Services

This document assumes that you decided to download my Pre Made image from [here](http://service.retro.lisias.net) . As a bonus, [HTTP2.BASH](https://bitbucket.org/Lisias/httpd2.bash) is also installed and configured. :-)

If you choose to build your own box from scratch, or reuse an appliance you already have working, please read [INSTALL](./INSTALL.md) first. At this point, the **only** difference should be on the user used to log in on the system: the Pre Made image uses the default Raspbian user for configuring the system `pi` - even the password is still the default `raspberry` (***CHANGE IT AS SOON AS YOU MANAGE TO***).

The services were designed to be run by an unprivileged user, so all the ports used are above 1024. All of them are between 8000 and 8999.

Since it's expected that these services will run on diskless stations, no logging facilities were implemented, the services spit text on STDOUT and STDERR. You can wrap them into a SHELL pipe if you need to save logs, I prefer to just startup them using SCREEN and log by SSH on the server to give a PEEK on that they are doing now and then).

If you want logs on disk, drop me a mail and I will help you on it. But I strongly advise against, as logging is one of the factors that drops the lifespan of the SD on Raspberry Pi et all.

## UPDATING the System (Raspbian only)

	ssh pi@raspberry
			(if you didn't renamed the box)
	sudo -i
	apt-get update
			(tons of output)
	apt-get upgrade
			(tons of output)
	apt-get dist-upgrade
			(tons of output)
	reboot

### Optional : Getting rid of the SystemD (Jessie only)

That' it, I got fed up. Things runs better on my Raspberries Pi by removing SystemD and putting back the sysvinit.

Be advised, however, that this works fine only in Jessie (Stretch got screwed up when I tried this stunt), and I had tested it extensively only on my Raspberries Pi. You will lost, also, all your X Desktops as everything is marked as having dependencies from SystemD. =/

	apt-get install sysvinit-core sysvinit-utils
	# The following line is not needed on my Jessie as it appears
	#cp /usr/share/sysvinit/inittab /etc/inittab

Edit/create `/etc/default/grub` as follows:

	GRUB_CMDLINE_LINUX_DEFAULT="init=/lib/sysvinit/init console=hvc0 console=ttyS0"

After that, you need:

	sudo apt-get install --reinstall grub
	sudo update-grub
	sudo reboot

And then:
	sudo -i
	apt-get remove --purge --auto-remove systemd
	echo -e 'Package: systemd\nPin: release *\nPin-Priority: -1' > /etc/apt/preferences.d/systemd

[source](http://without-systemd.org/wiki/index.php/How_to_remove_systemd_from_a_Debian_jessie/sid_installation)

All of this can render your Debian box somewhat harsh to be used, so it's not something to be done without some considerations. But if your Jessie box is going to be used *only* to serve the Confederation, you will be good by doing that.

On the other hand, [Devuan](https://devuan.org/) is stable nowadays (but still with some glitches). If you want to run this on a x86 machine, give it a try (but avoid using EXT4 for the time being, some packages were not updated to handle it yet).

From **my** point of view, things became more stable and less prone to random faults while power on and power off. The boot process became somewhat slower, however.


## UPDATING the Services

Chances are that I did update the [net.lisias.retro.PUB](https://bitbucket.org/Lisias/net.lisias.retro.pub) repository since I created the current SD Image, so it's a good measure to update the local repository:

	ssh pi@raspberry
			(if you didn't renamed the box)
	sudo -Hiu srv
	cd net.lisias.retro/pub
	git fetch ; git reset --hard origin/master

If a new egg file was fetched (see below, right after the string `README.md`),

```
srv@raspberrypi:~/net.lisias.retro.pub $ git pull
remote: Counting objects: 17, done.
remote: Compressing objects: 100% (16/16), done.
remote: Total 17 (delta 8), reused 0 (delta 0)
Unpacking objects: 100% (17/17), done.
From https://bitbucket.org/Lisias/net.lisias.retro.pub
6488665..6927ecf  master     -> origin/master
 * [new tag]         RELEASE.2016-1118.FIRST_BLOOD -> RELEASE.2016-1118.FIRST_BLOOD
Updating 6488665..6927ecf
Fast-forward
CONFIGURE.md                        | 221 ++++-----------------------------------------------------------------------------------------------------------
Docs/Archives.ods                   | Bin 17673 -> 17257 bytes
INSTALL.md                          | 239 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++------
README.md                           |   2 +-
net.lisias.retro.WS-1.0-py3.4.egg   | Bin 165965 -> 0 bytes
net.lisias.retro.WS-1.0.1-py3.4.egg | Bin 0 -> 176925 bytes
6 files changed, 236 insertions(+), 226 deletions(-)
delete mode 100644 net.lisias.retro.WS-1.0-py3.4.egg
create mode 100644 net.lisias.retro.WS-1.0.1-py3.4.egg
```

you need to reinstall the egg file as follows:

	source ~/python/bin/activate
	easy_install ./net.lisias.retro.WS-xx.yy.zz-py3.4.egg

where xx.yy.zz is the new version to be installed. There's nothing that can go wrong here (yeah... right... :-) ), as it will be always *only one** egg file on this directory.

It's also a good idea to update and upgrade your Raspbian box (no matter you're using my pre made SD or not):

	sudo apt-get update -y
	sudo apt-get upgrade -y
	sudo apt-get dist-upgrade -y
	sudo rpi-update

From now on, you are up to date.

## COMMON

Now we need to edit the file to fill your needs.

	sudo -Hiu srv (if you had logout)
	cd net.lisias.retro/pub
	vi configuration

You will get a screen as follows

```bash
#!/usr/bin/env bash

export BIND_HOST=RPI
export PUBLIC_ADDRESS=http://home.lisias.net
~
~
~
~
~
~
~
~
"configuration" [noeol] 4L, 86C
```

First, be cautious to **do not** leave spaces before and after the **=** sign. For some arcane reason, bash have trouble with spaces there.

You will need to change the `BIND_HOST` to your server's local IP (you can use the server's name, if you have a DNS server on your intranet). It's relatively simple to get this value if you don't, open another SSH session to your server and type:

	ifconfig eth0

```
ifconfig eth0
eth0      Link encap:Ethernet  HWaddr b8:27:eb:07:88:80
		inet addr:192.168.100.150  Bcast:192.168.100.255  Mask:255.255.255.0
		inet6 addr: xxxx:yyyy:a067::50/128 Scope:Global
		inet6 addr: xxxx:yyyy:6a:a06d:b578:456e:344f:cc85/64 Scope:Global
		inet6 addr: xxxx:yyyy:e6:f698/64 Scope:Link
		inet6 addr: xxxx:yyyy:a067:0:b117:1de7:f045:9d1f/64 Scope:Global
		UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
		RX packets:8267122 errors:0 dropped:0 overruns:0 frame:0
		TX packets:7858275 errors:0 dropped:0 overruns:0 carrier:0
		collisions:0 txqueuelen:1000
		RX bytes:960775081 (916.2 MiB)  TX bytes:1975792281 (1.8 GiB)
```
Look for the "inet addr" (192.168.100.150 on the example). This is the IP you need to bind the service into. Type it into the BIND_HOST line.

If your server is exposed directly to the Internet and have a DNS entry, you can type the host's name directly instead of using the IP - it's easier to maintain. Since my home router provides this service, I named the machine RPI and let the network handle the mess for me.

If your server is behind a NAT, you will need the NAT server's external address (or name) to configure the NETLOC configuration option. The NETLOC is how the outer world will call you to use the services. A full discussion about NAT can be found [here](./INSTALL.md#NAT).

You will need to edit the `PUBLIC_ADDRESS` of your server. This is the address people out there will use to talk to your Provider's host. Usually and intently, it will be a small appliance on your home's intranet, so you will provide your Router's IP, as explained before. You can use the IP given by the site iplocation.net , or you can (preferably) can use a dynamic DNS tool as http://dyn.com/ (I don't use this service, I just Google for one and get the first). I have my own DNS tool, so http://home.lisias.net always pinpoints to my home's IP even when it changes. It's practical, and I recommend such a solution for you.

### The Search Engines

This is what we are really talking about. Serving search engines are somewhat expensive, as they use a LOT of RAM. A 512Mb Raspberry Pi cannot serve all of them, but a Model 2 (that have 1 Gb) can.

So, probably, your first harsh decision will be "what archives I will provide?". If you are using a powerful enough appliance, please provide them all. But if you need to choose, here comes the information you will need.

On your browser, go to the URL http://service.retro.lisias.net/report . You will get something as:

```
net.lisias.retro Confederation of Retrocomputing Micro Services.

REQUEST: GET http://home.lisias.net:8090/report.txt

archives :
	funet.atari :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1
	wos :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	nvg.bbc :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	funet.msx :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1
	hobbes :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	nvg.sinclair :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	simtel :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1
	nvg.samcoupe :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	x2ftp :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	gaby :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	postgres :
		providers : {'http://home.lisias.net:8051/'}
		providers_count : 1
	garbo :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	maria :
		providers : {'http://home.lisias.net:8052/'}
		providers_count : 1
	aminet :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	funet.amiga :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1
	tv-dog :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	nvg.hw :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	nvg.sounds :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	metalab.pdp :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	webradio :
		providers : {'http://home.lisias.net:8060/'}
		providers_count : 1
	freedos :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	c64.padua :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	c64.arnold :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	hornet :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1
	whtech :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	nvg.vms :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	nvg.cpc :
		providers : {'http://home.lisias.net:8070/'}
		providers_count : 1
	a2.asimov :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	modland :
		providers : {'http://home.lisias.net:8073/'}
		providers_count : 1
	amigascne :
		providers : {'http://home.lisias.net:8071/'}
		providers_count : 1


To get help : http://service.retro.lisias.net/
```

Once selected the archives you plan to serve, edit the `net_lisias_retro_file_search_SERVED_ARCHIVES` variable from `~/configuration` file as follows:

```bash
# Set the minimum compress ratio on Descriptions when building datasets
# Will use less memory on the appliances to hold the datasets, but will take longer to access the descriptions as they will be decompressed by demand
# 0.0 will allow any level of compression
# 0.5 will use compression only when the compressed data is at least half the size of the original.
# 1.0 virtually prevent any compression, but wastes CPU as the Miners compress the data to check the sizes!
# Use None to do not use compression at all. Will save a lot of CPU cycles.
export net_lisias_retro_file_miner_MININUM_COMPRESS_LEVEL=None

# All known datasources listed by Package Name.
# Needed only if you want to use the `rebuild*.sh`, `run_search_update.sh` and `run_search_load_bench.sh` scripts.
export net_lisias_retro_file_ALL_ARCHIVES="A2.Asimov A2.DocProj AmigaScne AmiNet C64.Arnold C64.Padua Freedos funet.Amiga funet.Atari funet.Msx Gaby Garbo Hobbes Hornet ModLand metalab.PDP msx.Vitrola nvg.BBC nvg.CPC nvg.Hardware nvg.SamCoupe nvg.Sinclair nvg.Sounds nvg.VMS PigwaNet SceneOrg Simtel TvDog WhTech Wos x2ftp"

# A SET of the Archives to serve. None to serve *ALL*. "[]" serves no one (pretty useless).
# Current Archives are : ['a2.asimov', 'a2.doc.proj', 'amigascne', 'aminet', 'c64.arnold', 'c64.padua', 'freedos', 'funet.amiga', 'funet.atari', 'funet.msx', 'gaby', 'garbo', 'hobbes', 'hornet', 'metalab.pdp', 'modland', 'msx.Vitrola', 'nvg.bbc', 'nvg.cpc', 'nvg.hw', 'nvg.samcoupe', 'nvg.sinclair', 'nvg.sounds', 'nvg.vms', 'pigwa_net', 'scene_org', 'simtel', 'tv-dog', 'whtech', 'wos', 'x2ftp']
# Since there's no enough memory on a 512Mb RPi for everything, suggested configurations follows.
# Feel free to alter the suggestions to better fit your interests.
#export net_lisias_retro_file_search_SERVED_ARCHIVES="['gaby', 'whtech', 'simtel', 'modland', 'funet.msx', 'funet.amiga', 'funet.atari']"
#export net_lisias_retro_file_search_SERVED_ARCHIVES="['amigascne', 'aminet', 'wos']"
export net_lisias_retro_file_search_SERVED_ARCHIVES="[]"

export PYTHONPATH=$(dirname `realpath $BASH_SOURCE`)
```

The default value is `"[]"`, what serves nothing. Add the archives you want to serve following the examples. You can serve everything by using:

	export net_lisias_retro_file_search_SERVED_ARCHIVES=None

But be advised that a ton of memory will be needed! (about 5G for now). **Do not exceed the memory available from your appliance**. Using the swapfile will severily impair the performance, and will terribly cut down the lifespan of the SD Card if you are using a Raspberry Pi or another single board computer.

I would suggest that you priorize archives with fewer Providers available (see the `count` field). If one available archives is not served (i.e., it's not listed), by all means, please serve it. :)

A Libre Office spreadsheet is available [here](./Docs/Archives.ods) to help you to decide how to fit this in your appliance. The values are approximate (it's from my Raspberry Pi 1 Model B running Raspbian Jessie, where possible), your mileage will vary a lot.

Since each Archives have its own price in memory and time to get up, the spreadsheet will help you to achieve the best balance.


