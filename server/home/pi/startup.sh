#!/usr/bin/env bash

# Delete or comment the following lines after configuring your Providers!
echo "You need to configure your Providers."
echo "Visit http://service.retro.lisias.net for details"
exit
## Until here.

screen -D -RR DAEMONS -X quit || true
screen -dmS DAEMONS

screen -r DAEMONS -X source /home/srv/nat.session

# Uncomment next line to fire up HTTP2.BASH
# screen -r DAEMONS -X source /home/http/screen.session

# Uncomment next list to fire up net.lisias.retro.file.search
# screen -r DAEMONS -X source /home/srv/file-search.screen.session

# Uncomment next list to fire up net.lisias.retro.file.mpd
# screen -r DAEMONS -X source /home/srv/file-mpd.screen.session

