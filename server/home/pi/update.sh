#!/usr/bin/env bash

# Delete or comment the following lines after configuring your Providers!
echo "You need to configure your Providers."
echo "Visit http://service.retro.lisias.net for details"
exit
## Until here.

sudo -Hiu srv <<EOF
	cd ~/net.lisias.retro/pub/
	git fetch ; git reset --hard origin/master
	source ~/python/bin/activate
	easy_install net.lisias.retro.WS-1.1.0-py3.4.egg
EOF
