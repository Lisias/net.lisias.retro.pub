#!/usr/bin/env bash
echo Firing up net.lisias.retro.file.mpd
source ~/configuration
source ~/python/bin/activate
cd ~/net.lisias.retro/pub
python3 ./run_mpd_ws.py