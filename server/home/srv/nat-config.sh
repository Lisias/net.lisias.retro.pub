#!/usr/bin/env bash
echo Configuring your intranet\'s nat
source ~/configuration

if [ "y." -ne "${net_lisias_retro_NAT_UPNP}." ] ; then
	echo "Disabled."
	exit 0
fi

if [ "." -ne "${net_lisias_retro_proxy_BIND_PORT}." ] ; then
	echo "Setting up Proxy's NAT"
	upnpc -a ${BIND_HOST} ${net_lisias_retro_proxy_BIND_PORT} ${net_lisias_retro_proxy_BIND_PORT} tcp
fi

if [ "." -ne "${net_lisias_retro_file_search_BIND_PORT}." ] ; then
	echo "Setting up Search Engine's NAT"
	upnpc -a ${BIND_HOST} ${net_lisias_retro_file_search_BIND_PORT} ${net_lisias_retro_file_search_BIND_PORT} tcp
fi

if [ "." -ne "${net_lisias_retro_radio_BIND_PORT}." ] ; then
	echo "Setting up Web Radio's NAT"
	upnpc -a ${BIND_HOST} ${net_lisias_retro_radio_BIND_PORT} ${net_lisias_retro_radio_BIND_PORT} tcp
fi

if [ "." -ne "${net_lisias_retro_db_BIND_PORT}." ] ; then
	echo "Setting up DB Service's NAT"
	upnpc -a ${BIND_HOST} ${net_lisias_retro_db_BIND_PORT} ${net_lisias_retro_db_BIND_PORT} tcp
fi

echo "done."
